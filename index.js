// 1. Import express using require directive
const express = require("express");

// 2. Initialize express by using it to the app varibale as a function
const app = express();

// 3. Set the port number
const port = 4004;

// 4. Use middleware to allow express to read JSON
app.use(express.json());

// 5. Use middleware to allow express to be able to read more data types from a response
app.use(express.urlencoded({extended: true}));

// 6. Listen to the port and console.log a text once the server is running.
app.listen(port,() => console.log(`Server is running at localhost: ${port}`));


// [ SECTION ] Routes
	// Express has methods corresponding to each HTTP method

app.get("/hello", (request,response) => {
	response.send('Hello from the /hello endpoint');
});

app.post("/display-name",(request,response) => {
	response.send(`Hello there ${request.body.firstName} ${request.body.lastName}!`);
});

// [ SUB-SECTION ] Sign-up Form
let users = [];

app.post("/sign-up",(req,res) =>{
	if(req.body.username != "" && req.body.password != ""){
		users.push(req.body);
		res.send(`User ${req.body.username} successfully registered!`);
	} else {
		res.send("Please input BOTH username and password!");
	}
});

app.put("/change-password",(req,res) => {
	
	// Creates a variable to store the message to be sent back to the client/Postman
	let message;

	// Creates a for loop that will loop through the elements of the "users" array
	for(let i = 0; i < users.length; i++){
		
		// If the username provided in the client/Postman and the username of the current object in the loop is the same
		if(req.body.username == users[i].username){
			
			// Changes the password of the user found by the loop into the password provided in the client/Postman
			users[i].password = req.body.password;
			
			// Changes the message to be sent back by the response.
			message = `User ${req.body.username}'s password has been updated.`;
			
			// Breaks out of the loop once a user that matches the username provided in the client/Postman is found.
			break;

			// If no user was found
		}else{
			
			// Changes the message to be sent back by the response.
			message = "User does not exists!";
		}
	}

	// Send a response back to the client/Postman once the password has been updated or if a user is not found.
	res.send(message);
})

// [ ACTIVITY ]
	// Setting a users
	users = [{
		"username" : "Mavis",
		"password" : "123amdin"
	}];

//1. displaying message to home endpoint route
app.get("/home", (req,res) => {
	res.send('Welcome to home page');
});

//2. retrieving all users from the users route
app.get("/users", (req, res) => {
	res.send(users);

});

// 3. Deleting a user from the delete route
app.delete("/delete-user",(req, res) =>{
	for(i = 0; i < users.length; i++){
		if(req.body.username == users[i].username){
			message = `User ${req.body.username} has been deleted.`;
			break;
		}else{
			message = `There's nothing to delete since there are no users found.`;
		}
	}
	res.send(message);
})
